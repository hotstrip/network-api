// Chrome DevTools Extension中不能使用console.log
const log = (...args) => chrome.devtools.inspectedWindow.eval(`console.log(...${JSON.stringify(args)});`);

window.onload = (() => {
  document.getElementById("check_jquery").addEventListener("click", function () {
    chrome.devtools.inspectedWindow.eval(
      "jQuery.fn.jquery",
      function (result, isException) {
        if (isException) {
          log("the page is not using jQuery");
        } else {
          log("The page is using jQuery v" + result);
        }
      }
    );
  });
  
  document.getElementById("get_all_resources").addEventListener("click", function () {
    chrome.devtools.inspectedWindow.getResources(function (resources) {
      log(resources);
    });
  });
})

// 注册回调，每一个http请求响应后，都触发该回调
chrome.devtools.network.onRequestFinished.addListener(async (req) => {
  try {
    log(req.request.method);
    if (req.request.method === 'POST') {
      // 将callback转为await promise
      // warn: content 在 getContent 回调函数中，而不是 getContent 的返回值
      const content = await new Promise(res => req.getContent(res));
      log(req.response)
      log('content: ' + content);
    }
  } catch (err) {
    log(err.stack || err.toString());
  }
});

