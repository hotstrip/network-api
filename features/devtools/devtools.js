// Chrome DevTools Extension中不能使用console.log
const log = (...args) => chrome.devtools.inspectedWindow.eval(`console.log(...${JSON.stringify(args)});`);

// 创建自定义面板，同一个插件可以创建多个自定义面板
// 几个参数依次为：panel标题、图标（其实设置了也没地方显示）、要加载的页面、加载成功后的回调
chrome.devtools.panels.create('Network Api', null, 'features/devtools/panels/api-panel.html', (panel) => {
  // 注册回调，每一个http请求响应后，都触发该回调
  // chrome.devtools.network.onRequestFinished.addListener(async (req) => {
  //   try {
  //     log(req.request.method);
  //     if (req.request.method === 'POST') {
  //       // 将callback转为await promise
  //       // warn: content 在 getContent 回调函数中，而不是 getContent 的返回值
  //       const content = await new Promise(res => req.getContent(res));
  //       log(req.response)
  //       log('content: ' + content);
  //     }
  //   } catch (err) {
  //     log(err.stack || err.toString());
  //   }
  // });
});


window.onload = function() {
  
}