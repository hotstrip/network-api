let color = '#3aa757';

// 添加监听
chrome.runtime.onInstalled.addListener((message) => {
  chrome.storage.sync.set({ color });
  console.log('Default background color set to %cgreen', `color: ${color}`);

  console.log(message);
});